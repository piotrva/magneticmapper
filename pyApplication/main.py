from PyQt5 import QtWidgets, uic
from PyQt5.QtWidgets import *
from PyQt5.QtCore import QThread, QObject, pyqtSignal
import matplotlib.pyplot as plt
import matplotlib
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg, NavigationToolbar2QT
from mpl_toolkits.mplot3d import Axes3D  # noqa: F401 unused import
import numpy as np
import sys
#import threading
from queue import Queue
import serial
import serial.tools.list_ports
import time
from operator import add
import json

class StageXY:
    port = 0
    serial = serial.Serial()

    def set_port(self, port):
        self.port = port

    def connect(self):
        self.serial = serial.Serial(
            port=self.port,
            baudrate=115200,
            parity=serial.PARITY_NONE,
            stopbits=serial.STOPBITS_ONE,
            bytesize=serial.EIGHTBITS,
            timeout=1
        )
        QThread.msleep(2000)

        print(self.serial.read_until().decode().strip())

        print(self.serial.read_until().decode().strip())

        return self.serial.isOpen()

    def disconnect(self):
        if self.serial.isOpen():
            self.serial.close()

    def isConnected(self):
        return self.serial.isOpen()

    def waitReady(self, timeout = 5):
        QThread.msleep(200)
        if not self.isConnected():
            return
        notReady = True
        start = time.time()
        while notReady:
            QThread.msleep(100)
            if time.time() - start > timeout:
                break
            self.serial.write(str.encode("?"))
            response = self.serial.read_until().decode().strip()
            response = response.split("|")
            if len(response)>1:
                notReady = response[0] != "<Idle"

    def jog(self, x, y):
        if not self.isConnected():
            return
        self.serial.write(str.encode("$J=G21G91X" + str(x) + "Y" + str(y) + "F1000\n"))
        print(self.serial.read_until().decode().strip() == "ok")
        self.waitReady()

    def zero(self):
        if not self.isConnected():
            return
        self.serial.write(str.encode("G10 P0 L20 X0 Y0 Z0\n"))
        print(self.serial.read_until().decode().strip() == "ok")

    def move(self, x, y, speed=1000):
        if not self.isConnected():
            return
        self.serial.write(str.encode("G21 G90 G17 G1 X" + str(x) + " Y" + str(y) + " F" + str(int(speed)) + "\n"))
        print(self.serial.read_until().decode().strip() == "ok")
        self.waitReady()

    def lift(self, value):
        if not self.isConnected():
            return
        if value < 17:
            value = 17
        elif value > 28:
            value = 28

        self.serial.write(str.encode("M3S" + str(value) + "\n"))
        print(self.serial.read_until().decode().strip() == "ok")


class Magnetometer:
    port = 0
    serial = serial.Serial()

    def set_port(self, port):
        self.port = port

    def connect(self):
        self.serial = serial.Serial(
            port=self.port,
            baudrate=115200,
            parity=serial.PARITY_NONE,
            stopbits=serial.STOPBITS_ONE,
            bytesize=serial.EIGHTBITS,
            timeout=1
        )
        QThread.msleep(2000)

        return self.serial.isOpen()

    def disconnect(self):
        if self.serial.isOpen():
            self.serial.close()

    def isConnected(self):
        return self.serial.isOpen()

    def read(self):
        if not self.isConnected():
            return {"x": 0.0, "y": 0.0, "z": 0.0}
        self.serial.write(str.encode("r\n"))
        received = self.serial.read_until().decode()
        received = received.split(";")
        if len(received) < 4:
            return {"x": 0.0, "y": 0.0, "z": 0.0}
        else:
            to_return = []
            for i in range(0,3):
                to_return.append(float(received[i])/np.power(1.0755, float(received[3]) - 20.0))
            #return {"x": to_return[0], "y": to_return[1], "z": to_return[2]}
            return {"y": -to_return[0], "z": to_return[1], "x": -to_return[2]*1.2}


class Worker(QThread):

    finished = pyqtSignal()  # give worker class a finished signal
    magnetometer = Magnetometer()
    stage = StageXY()

    def __init__(self, gui, parent=None):
        QThread.__init__(self, parent=parent)
        self.continue_run = True  # provide a bool run condition for the class
        self.msg_in = gui.msg
        self.gui = gui
        self.continue_meas = True

    def do_work(self):
        while self.continue_run:  # give the loop a stoppable condition
            QThread.msleep(25)
            if not self.msg_in.empty():
                message = self.msg_in.get()
                if message[0]=="jog":
                    multiplier = float(self.gui.JogValue.text())
                    self.stage.jog(multiplier * message[1], multiplier * message[2])
                elif message[0]=="connect":
                    if message[1]:
                        self.magnetometer.set_port(self.gui.MagnetometerPortSel.currentText())
                        self.magnetometer.connect()
                        self.stage.set_port(self.gui.StagePortSel.currentText())
                        self.stage.connect()
                        self.stage.lift(int(self.gui.LiftEdit.value()))
                    else:
                        self.magnetometer.disconnect()
                        self.stage.disconnect()
                elif message[0] == "stagezero":
                    self.stage.zero()
                elif message[0] == "lift":
                    self.stage.lift(28)
                    QThread.msleep(500)
                    self.stage.lift(17)
                    QThread.msleep(500)
                    self.stage.lift(int(self.gui.LiftEdit.value()))
                elif message[0] == "measure":
                    self.continue_meas = True
                    xmax = float(self.gui.XSizeEdit.value())
                    ymax = float(self.gui.YSizeEdit.value())
                    step = float(self.gui.StepEdit.value())

                    xrange = list(np.arange(0, xmax + step/2, step))
                    yrange = list(np.arange(0, ymax + step/2, step))
                    if step != 0:
                        self.gui.clearData()
                        for x in xrange:
                            for y in yrange:
                                self.stage.move(x, y)
                                QThread.msleep(100)
                                data = self.magnetometer.read()
                                self.gui.statusbar.showMessage(str(data))
                                self.gui.appendData(x, y, 0, data["x"], data["y"], data["z"])
                                self.gui.plotUpdate()
                                if not self.continue_meas:
                                    break
                            yrange.reverse()
                            if not self.continue_meas:
                                break
                    self.stage.move(0, 0)
                elif message[0] == "gostart":
                    self.stage.move(0, 0)
                elif message[0] == "preview":
                    xmax = float(self.gui.XSizeEdit.value())
                    ymax = float(self.gui.YSizeEdit.value())
                    self.stage.move(0, 0)
                    self.stage.move(xmax, 0)
                    self.stage.move(xmax, ymax)
                    self.stage.move(0, ymax)
                    self.stage.move(0, 0)
        self.finished.emit()  # emit the finished signal when the loop is done

    def stop(self):
        self.continue_run = False  # set the run condition to false on stop

    def breakMeasurement(self):
        self.continue_meas = False


class MainWindow(QMainWindow):
    stop_signal = pyqtSignal()
    break_signal = pyqtSignal()

    Xdata = [0, 0, 0]
    Ydata = [0, 1, 2]
    Zdata = [0, 0, 0]
    Udata = [1, 0, 0]
    Vdata = [1, 1, 0]
    Wdata = [1, 1, 1]

    def __init__(self, *args):
        super(MainWindow, self).__init__(*args)
        uic.loadUi("GUI.ui", self)
        self.show()
        self.setWindowTitle("Magnetic Field Mapper")

        self.step = float(self.StepEdit.value())

        self.fig1 = plt.Figure()
        self.ax1f1 = self.fig1.add_subplot(111, projection='3d')
        #self.plotHandle, = self.ax1f1.plot([])

        # Make the grid
        self.plotHandle = self.ax1f1.quiver3D(self.Xdata, self.Ydata, self.Zdata, self.Udata, self.Vdata, self.Wdata,
                                              length=float(self.ArrowEdit.value()), normalize=False)

        self.canvas = FigureCanvasQTAgg(self.fig1)
        self.fig1.tight_layout()
        self.ax1f1.mouse_init()
        self.PlotBoxLayout.addWidget(self.canvas)
        self.canvas.draw()

        self.fig2 = plt.Figure()
        self.ax1f2 = self.fig2.add_subplot(111, aspect='equal')

        # self.plotHandle, = self.ax1f1.plot([])

        # Make the grid

        Z = list(map(add, np.power(self.Udata, 2), np.power(self.Vdata, 2)))
        Z = list(map(add, Z, np.power(self.Wdata, 2)))
        Z = list(np.power(Z, 0.5))

        self.plotHandle2 = self.ax1f2.scatter(self.Xdata, self.Ydata, cmap = 'viridis', c=Z)
        self.colorbarHandle = self.fig2.colorbar(self.plotHandle2)

        self.canvas2 = FigureCanvasQTAgg(self.fig2)
        #self.canvas2.mpl_connect('draw_event', lambda x: self.colormapResize())
        self.ax1f2.callbacks.connect('xlim_changed', lambda x: self.colormapResize())
        #ax.callbacks.connect('ylim_changed', on_ylims_change)
        self.fig2.tight_layout()
        #self.ax1f2.mouse_init()
        self.PlotBoxLayout2.addWidget(self.canvas2)
        self.canvas2.draw()

        self.toolbar = NavigationToolbar2QT(self.canvas2, self.PlotBox2, coordinates=True)
        self.PlotBoxLayout2.addWidget(self.toolbar)

        self.plotUpdate()
        self.scalePlot()

        self.JogDown.clicked.connect(lambda: self.sendJog(0, -1))
        self.JogDownLeft.clicked.connect(lambda: self.sendJog(-1, -1))
        self.JogDownRight.clicked.connect(lambda: self.sendJog(1, -1))
        self.JogUp.clicked.connect(lambda: self.sendJog(0, 1))
        self.JogUpLeft.clicked.connect(lambda: self.sendJog(-1, 1))
        self.JogUpRight.clicked.connect(lambda: self.sendJog(1, 1))
        self.JogRight.clicked.connect(lambda: self.sendJog(1, 0))
        self.JogLeft.clicked.connect(lambda: self.sendJog(-1, 0))

        self.ConnectBtn.clicked.connect(lambda: self.connect())
        self.ScanButton.clicked.connect(lambda: self.scanPorts())
        self.StartPointButton.clicked.connect(lambda: self.stageZero())
        self.StartButton.clicked.connect(lambda: self.startMeasurement())

        self.ZMin.valueChanged.connect(lambda: self.scalePlot())
        self.ZMax.valueChanged.connect(lambda: self.scalePlot())
        self.ArrowEdit.valueChanged.connect(lambda: self.plotUpdate())

        self.GoStartButton.clicked.connect(lambda: self.sendGoStart())
        self.PreviewButton.clicked.connect(lambda: self.sendPreview())
        self.BreakButton.clicked.connect(lambda: self.sendBreak())

        self.ViewMode.currentIndexChanged.connect(lambda: self.plotUpdate())
        self.LiftEdit.valueChanged.connect(lambda: self.lift())

        self.LoadButton.clicked.connect(lambda: self.loadData())
        self.SaveButton.clicked.connect(lambda: self.saveData())

        self.MagnetometerPortSel.addItem("COM59")
        self.StagePortSel.addItem("COM40")

        self.listPorts()


        #self.plotHandle.set_xdata([1, 2, 3])
        #self.plotHandle.set_ydata([1, 2, 3])
        #self.fig1.axes[0].relim()
        #self.fig1.axes[0].autoscale_view()

        self.msg = Queue()
        # Thread:
        self.thread = QThread()
        self.worker = Worker(self)
        self.stop_signal.connect(self.worker.stop)  # connect stop signal to worker stop method
        self.break_signal.connect(self.worker.breakMeasurement)
        self.worker.moveToThread(self.thread)

        self.worker.finished.connect(self.thread.quit)  # connect the workers finished signal to stop thread
        self.worker.finished.connect(self.worker.deleteLater)  # connect the workers finished signal to clean up worker
        self.thread.finished.connect(self.thread.deleteLater)  # connect threads finished signal to clean up thread

        self.thread.started.connect(self.worker.do_work)
        self.thread.finished.connect(self.worker.stop)

        self.thread.start()

    def clearData(self):
        self.Xdata.clear()
        self.Ydata.clear()
        self.Zdata.clear()
        self.Udata.clear()
        self.Vdata.clear()
        self.Wdata.clear()

    def appendData(self, x, y, z, u, v, w):
        self.Xdata.append(x)
        self.Ydata.append(y)
        self.Zdata.append(z)
        self.Udata.append(u)
        self.Vdata.append(v)
        self.Wdata.append(w)

    def saveData(self):
        fname = QFileDialog.getSaveFileName(self, 'Save file',
                                            'c:\\', "JSON files (*.json)")
        data = {}
        data['x'] = self.Xdata
        data['y'] = self.Ydata
        data['z'] = self.Zdata
        data['u'] = self.Udata
        data['v'] = self.Vdata
        data['w'] = self.Wdata
        data['step'] = self.step
        if fname[0] == '':
            return
        with open(fname[0], 'w') as fp:
            json.dump(data, fp)

    def loadData(self):
        fname = QFileDialog.getOpenFileName(self, 'Open file',
                                            'c:\\', "JSON files (*.json)")
        if fname[0] == '':
            return
        self.clearData()
        with open(fname[0], 'r') as fp:
            data = json.load(fp)
            self.Xdata = data['x']
            self.Ydata = data['y']
            self.Zdata = data['z']
            self.Udata = data['u']
            self.Vdata = data['v']
            self.Wdata = data['w']
            self.step = data['step']
        self.ax1f1.set_xlim3d(0, max(self.Xdata))
        self.ax1f1.set_ylim3d(0, max(self.Ydata))
        self.ax1f2.set_xlim(0, max(self.Xdata))
        self.ax1f2.set_ylim(0, max(self.Ydata))
        self.plotUpdate()
        self.scalePlot()


    def scalePlot(self):
        self.ax1f1.set_zlim3d(float(self.ZMin.value()), float(self.ZMax.value()))
        self.canvas.draw()

    def plotUpdate(self):
        mode = self.ViewMode.currentIndex()

        Z = 0

        if mode == 0:
            Z = list(map(add, np.power(self.Udata, 2), np.power(self.Vdata, 2)))
            Z = list(map(add, Z, np.power(self.Wdata, 2)))
            Z = list(np.power(Z, 0.5))
        elif mode == 1:
            Z = list(map(add, np.power(self.Udata, 2), np.power(self.Vdata, 2)))
            Z = list(map(add, Z, np.power(self.Wdata, 2)))
            Z = list(np.log(list(np.power(Z, 0.5))))
        elif mode == 2:
            Z = list(self.Wdata)
        elif mode == 3:
            Z = list(self.Wdata)
            Z = list(map(lambda x: x - min(Z) + 0.01, Z))
            Z = list(np.log(Z))
        elif mode == 4:
            Z = list(self.Udata)
        elif mode == 5:
            Z = list(self.Udata)
            Z = list(map(lambda x: x - min(Z) + 0.01, Z))
            Z = list(np.log(Z))
        elif mode == 6:
            Z = list(self.Vdata)
        elif mode == 7:
            Z = list(self.Vdata)
            Z = list(map(lambda x: x - min(Z) + 0.01, Z))
            Z = list(np.log(Z))

        c = np.array(Z)
        c = (c.ravel() - c.min()) / c.ptp()
        # Repeat for each body line and two head lines
        c = np.concatenate((c, np.repeat(c, 2)))
        # Colormap
        # c = plt.cm.hsv(c)
        cmap = matplotlib.cm.get_cmap('viridis')
        Zc = cmap(c)
        self.plotHandle.remove()
        q = self.plotHandle = self.ax1f1.quiver3D(self.Xdata, self.Ydata, self.Zdata, self.Udata, self.Vdata,
                                                  self.Wdata,
                                                  length=float(self.ArrowEdit.value()), normalize=False, colors=Zc)
        # q.set_array(np.array([ele for ele in Z for i in range(3)]))
        self.canvas.draw()

        self.colorbarHandle.remove()
        self.plotHandle2.remove()

        self.plotHandle2 = self.ax1f2.scatter(self.Xdata, self.Ydata, cmap='viridis', c=Z, marker="s", s=self.calculateSize())

        self.colorbarHandle = self.fig2.colorbar(self.plotHandle2)
        #self.forceAspect(self.ax1f2)
        self.canvas2.draw()

    def calculateSize(self):
        #nx = int(self.fig2.get_figwidth() * self.fig2.dpi)
        ny = int(self.fig2.get_figheight() * self.fig2.dpi)
        #xlim = self.ax1f2.get_xlim()
        ylim = self.ax1f2.get_ylim()

        return (ny / abs(ylim[0] - ylim[1]) / 2 * self.step)**2

    def colormapResize(self):
        size = self.calculateSize()
        self.plotHandle2.set_sizes([size] * len(self.Xdata))


    def startMeasurement(self):
        self.ax1f1.set_xlim3d(0, float(self.XSizeEdit.value()))
        self.ax1f1.set_ylim3d(0, float(self.YSizeEdit.value()))
        self.ax1f2.set_xlim(0, float(self.XSizeEdit.value()))
        self.ax1f2.set_ylim(0, float(self.YSizeEdit.value()))
        self.step = float(self.StepEdit.value())
        self.msg.put(("measure", 0))

    def stageZero(self):
        self.msg.put(("stagezero", 0))

    def listPorts(self):
        comports = serial.tools.list_ports.comports()
        for comport in comports:
            self.MagnetometerPortSel.addItem(comport.device)
            self.StagePortSel.addItem(comport.device)

    def scanPorts(self):
        self.MagnetometerPortSel.clear()
        self.StagePortSel.clear()
        self.listPorts()

    def connect(self):
        self.msg.put(("connect", self.ConnectBtn.isChecked()))

    def closeEvent(self, event):
        self.stop_signal.emit() #zatrzymamy proces w innym watku
        print("ZAMYKAMY!")

    def statusShow(self, text):
        self.statusbar.showMessage(text, 500)
        #self.msg.put(text)

    def sendJog(self, x, y):
        self.msg.put(("jog",x,y))

    def sendBreak(self):
        self.break_signal.emit()

    def sendGoStart(self):
        self.msg.put(("gostart", 0))

    def sendPreview(self):
        self.msg.put(("preview", 0))

    def lift(self):
        self.msg.put(("lift", 0))


app = QtWidgets.QApplication(sys.argv)
window = MainWindow()
sys.exit(app.exec_())
