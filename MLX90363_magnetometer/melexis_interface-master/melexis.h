/*
	MELEXIS.h - MELEXIS library

	For interfacing with the MLX90363 triaxis hall effect sensor
	
	by Travis Howse <tjhowse@gmail.com>
	2012.   License, GPL v2 or later

*/

#ifndef MELEXIS_h
#define MELEXIS_h

#include <inttypes.h>

/******************************************************************************
 * Definitions
 ******************************************************************************/
 

#define MELEXIS_GET1 0x13
#define MELEXIS_GET2 0x14
#define MELEXIS_GET3 0x15
#define MELEXIS_MemoryRead 0x01
#define MELEXIS_EEPROMWrite 0x03
#define MELEXIS_EEChallengeAns 0x05
#define MELEXIS_EEReadChallenge 0x0F
#define MELEXIS_NOP 0x10
#define MELEXIS_DiagnosticDetails 0x16
#define MELEXIS_OscCounterStart 0x18
#define MELEXIS_OscCounterStop 0x1A
#define MELEXIS_Reboot 0x2F
#define MELEXIS_Standby 0x31

#define MELEXIS_Get3Ready 0x2D
#define MELEXIS_MemoryReadAnswer 0x02
#define MELEXIS_EEPROMWriteChallenge 0x04
#define MELEXIS_EEReadAnswer 0x28
#define MELEXIS_EEPROMWriteStatus 0x0E
#define MELEXIS_Challenge 0x11
#define MELEXIS_DiagnosticsAnswer 0x17
#define MELEXIS_OscCounterStart 0x09
#define MELEXIS_OscCounterStopAck 0x1B
#define MELEXIS_StandbyAck 0x32
#define MELEXIS_Errorframe 0x3D
#define MELEXIS_NTT 0x3E
#define MELEXIS_ReadyMessage 0x2C

// Each EEPROM setting has an address, a bit offset and a length
#define MELEXIS_EE_MAPXYZ_A 0x102A
#define MELEXIS_EE_MAPXYZ_O 0
#define MELEXIS_EE_MAPXYZ_L 3
#define MELEXIS_EE_MAPXYZ MELEXIS_EE_MAPXYZ_A,MELEXIS_EE_MAPXYZ_O,MELEXIS_EE_MAPXYZ_L
#define MELEXIS_EE_3D_A 0x102A
#define MELEXIS_EE_3D_O 3
#define MELEXIS_EE_3D_L 1
#define MELEXIS_EE_3D MELEXIS_EE_3D_A,MELEXIS_EE_3D_O,MELEXIS_EE_3D_L
#define MELEXIS_EE_FILTER_A 0x102A
#define MELEXIS_EE_FILTER_O 4
#define MELEXIS_EE_FILTER_L 2
#define MELEXIS_EE_FILTER MELEXIS_EE_FILTER_A,MELEXIS_EE_FILTER_O,MELEXIS_EE_FILTER_L
#define MELEXIS_EE_VIRTUALGAINMAX_A 0x102E
#define MELEXIS_EE_VIRTUALGAINMAX_O 8
#define MELEXIS_EE_VIRTUALGAINMAX_L 8
#define MELEXIS_EE_VIRTUALGAINMAX MELEXIS_EE_VIRTUALGAINMAX_A,MELEXIS_EE_VIRTUALGAINMAX_O,MELEXIS_EE_VIRTUALGAINMAX_L
#define MELEXIS_EE_VIRTUALGAINMIN_A 0x102E
#define MELEXIS_EE_VIRTUALGAINMIN_O 0
#define MELEXIS_EE_VIRTUALGAINMIN_L 8
#define MELEXIS_EE_VIRTUALGAINMIN MELEXIS_EE_VIRTUALGAINMIN_A,MELEXIS_EE_VIRTUALGAINMIN_O,MELEXIS_EE_VIRTUALGAINMIN_L
#define MELEXIS_EE_KALPHA_A 0x1022
#define MELEXIS_EE_KALPHA_O 0
#define MELEXIS_EE_KALPHA_L 16
#define MELEXIS_EE_KALPHA MELEXIS_EE_KALPHA_A,MELEXIS_EE_KALPHA_O,MELEXIS_EE_KALPHA_L
#define MELEXIS_EE_KBETA_A 0x1024
#define MELEXIS_EE_KBETA_O 0
#define MELEXIS_EE_KBETA_L 16
#define MELEXIS_EE_KBETA MELEXIS_EE_KBETA_A,MELEXIS_EE_KBETA_O,MELEXIS_EE_KBETA_L
#define MELEXIS_EE_SMISM_A 0x1032
#define MELEXIS_EE_SMISM_O 0
#define MELEXIS_EE_SMISM_L 16
#define MELEXIS_EE_SMISM MELEXIS_EE_SMISM_A,MELEXIS_EE_SMISM_O,MELEXIS_EE_SMISM_L
#define MELEXIS_EE_ORTH_B1B2_A 0x1026
#define MELEXIS_EE_ORTH_B1B2_O 0
#define MELEXIS_EE_ORTH_B1B2_L 8
#define MELEXIS_EE_ORTH_B1B2 MELEXIS_EE_ORTH_B1B2_A,MELEXIS_EE_ORTH_B1B2_O,MELEXIS_EE_ORTH_B1B2_L
#define MELEXIS_EE_KT_A 0x1030
#define MELEXIS_EE_KT_O 0
#define MELEXIS_EE_KT_L 16
#define MELEXIS_EE_KT MELEXIS_EE_KT_A,MELEXIS_EE_KT_O,MELEXIS_EE_KT_L
#define MELEXIS_EE_PHYST_A 0x1028
#define MELEXIS_EE_PHYST_O 8
#define MELEXIS_EE_PHYST_L 8
#define MELEXIS_EE_PHYST MELEXIS_EE_PHYST_A,MELEXIS_EE_PHYST_O,MELEXIS_EE_PHYST_L
#define MELEXIS_EE_PINFILTER_A 0x1001
#define MELEXIS_EE_PINFILTER_O 0
#define MELEXIS_EE_PINFILTER_L 2
#define MELEXIS_EE_PINFILTER MELEXIS_EE_PINFILTER_A,MELEXIS_EE_PINFILTER_O,MELEXIS_EE_PINFILTER_L
#define MELEXIS_EE_USERID_H_A 0x103A
#define MELEXIS_EE_USERID_H_O 0
#define MELEXIS_EE_USERID_H_L 16
#define MELEXIS_EE_USERID_H MELEXIS_EE_USERID_H_A,MELEXIS_EE_USERID_H_O,MELEXIS_EE_USERID_H_L
#define MELEXIS_EE_USERID_L_A 0x103C
#define MELEXIS_EE_USERID_L_O 0
#define MELEXIS_EE_USERID_L_L 16
#define MELEXIS_EE_USERID_L MELEXIS_EE_USERID_L_A,MELEXIS_EE_USERID_L_O,MELEXIS_EE_USERID_L_L


class MELEXIS
{
	public:
		int16_t get_x();
		int16_t get_y();
		int16_t get_z();
		uint8_t get_diag();
		uint16_t get_diag_0();
		uint16_t get_diag_1();
		uint8_t get_roll();
		uint8_t get_marker();
		uint8_t poll();
		uint8_t angle_poll();
		uint8_t diag_poll();
		uint8_t do_SPI();
		uint8_t reboot();
		void send_NOP();
		
		uint16_t set_eeprom(uint16_t addr, uint8_t offset, uint8_t length, uint16_t data);
		uint16_t get_eeprom(uint16_t addr, uint8_t offset, uint8_t length);
		uint16_t get_eeprom_word(uint16_t addr, uint8_t offset, uint8_t length);
		uint16_t get_EE_Key(uint16_t addr);

		MELEXIS(uint8_t selectPin);
	
	private:
		bool do_checksum(uint8_t* message);

};

#endif

