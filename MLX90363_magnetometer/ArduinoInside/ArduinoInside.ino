#include <SPI.h>
#include "melexis.h"

MELEXIS test(10);

void setup()
{         
  /*delay(500);
  test.set_eeprom(MELEXIS_EE_VIRTUALGAINMAX, 41);
  delay(500);/*
  test.set_eeprom(MELEXIS_EE_VIRTUALGAINMIN, 0);
  delay(500);*/
  
  Serial.begin(115200);  
  Serial.print("VGAIN_MIN: ");
  Serial.print(test.get_eeprom(MELEXIS_EE_VIRTUALGAINMIN));
  Serial.print(" VGAIN_MAX: ");
  Serial.print(test.get_eeprom(MELEXIS_EE_VIRTUALGAINMAX));
  Serial.println();
  while(!Serial.available());
  
}

bool cycle = true;

int gain = 41;

void loop()
{
  delayMicroseconds(2500); //tSSRERE_mod3
  if(cycle){
    if(test.poll()){
      Serial.print("*");
    }else{
      Serial.print(" ");
    }
  }else{
    if(test.angle_poll()){
      Serial.print("*");
    }else{
      Serial.print(" ");
    }
  }
  cycle = !cycle;

  if(test.get_marker() == 2){
    for(int i = 0 ; i < 3 ; i++){
      float readout;
      switch(i){
        case 0:
        readout = test.get_x();
        Serial.print("X: ");
        break;
        case 1:
        readout = test.get_y();
        Serial.print(" Y: ");
        break;
        case 2:
        readout = test.get_z();
        Serial.print(" Z: ");
        break;
      }
      readout = readout/pow(1.0755,gain - 20);
      if(readout >= 0) Serial.print(" ");
      if(abs(readout) < 10) Serial.print(" ");
      if(abs(readout) < 100) Serial.print(" ");
      if(abs(readout) < 1000) Serial.print(" ");
      Serial.print(readout, 3);
    }
  }else if(test.get_marker() == 0){
    Serial.print(" G: ");
    int readout = test.get_z();
    if(readout >= 0) Serial.print(" ");
    if(abs(readout) < 10) Serial.print(" ");
    if(abs(readout) < 100) Serial.print(" ");
    if(abs(readout) < 1000) Serial.print(" ");
    Serial.print(readout, DEC);
    gain = readout;
    Serial.println();
  }
  
  

  

  /*Serial.print(" orig: ");
  Serial.println(test.get_eeprom(MELEXIS_EE_3D),BIN);
  Serial.print("write: ");
  Serial.println(test.set_eeprom(MELEXIS_EE_3D,1),DEC);
  Serial.print("  new: ");
  Serial.println(test.get_eeprom(MELEXIS_EE_3D),BIN);

  while (1)
  {
    Serial.print("  done ");
    delay(1000);
  }*/
  /*Serial.print("Diag0: ");
  Serial.println(test.get_diag_0(),DEC);
  Serial.print("Diag1: ");
  Serial.println(test.get_diag_1(),DEC);*/
  
}

