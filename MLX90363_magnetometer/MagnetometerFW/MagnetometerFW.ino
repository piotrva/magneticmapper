#include <SPI.h>
#include "melexis.h"

#include <CLI.h>

MELEXIS test(10);

CLI_COMMAND(readFunc);

void setup()
{         
  Serial.begin(115200);  
  CLI.setDefaultPrompt("");
  
  CLI.addCommand("r", readFunc);

  CLI.addClient(Serial);
  CLI.echoSet(0);
}

bool cycle = true;

int gain = 41;
int x = 0, y = 0, z = 0;

void loop()
{
  delayMicroseconds(2500); //tSSRERE_mod3
  if(cycle){
    test.poll();
  }else{
    test.angle_poll();
  }
  cycle = !cycle;

  if(test.get_marker() == 2){
    for(int i = 0 ; i < 3 ; i++){
      float readout;
      switch(i){
        case 0:
          x = test.get_x();
        break;
        case 1:
          y = test.get_y();
        break;
        case 2:
          z = test.get_z();
        break;
      }
    }
  }else if(test.get_marker() == 0){
    gain = test.get_z();
  }
  CLI.process(); 
}

CLI_COMMAND(readFunc) {
    dev->print(x);
    dev->print(";");
    dev->print(y);
    dev->print(";");
    dev->print(z);
    dev->print(";");
    dev->print(gain);
    dev->print(";\n");
    
    return 0;
}

